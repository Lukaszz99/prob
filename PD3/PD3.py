####################
#  Łukasz Sawicki  #
#    Prob - pd3    #
####################
import sys
import matplotlib.pyplot as plt
from random import seed
from random import randint

kostki_path = sys.argv[1]
wierzcholki_path = sys.argv[2]
randint_number = 0
if int(sys.argv[3]) > 10000:
    print("podana liczba " + sys.argv[3] + " do symulacji jest za duza, usawiam na 10000")
    randint_number = 10000
else: randint_number = int(sys.argv[3])

def licz_sum(list, n):
    value = 0
    for i in range(n):
        value += list[i]
    return value/n


read_data = open(kostki_path, "r").read()
kostki_dane = [int(k) for k in read_data if k in "123456"]

read_data = open(wierzcholki_path, "r").readlines()
wierzcholki = [int(k) for k in read_data]


#################################################
#bonus
seed(randint(1,1000000))
y = []
for i in range(randint_number):
    y.append(randint(1, 6))

plt.hist(y,6,edgecolor="black")
hist_title = "Histogram rzutu kostką dla " + str(randint_number) + " rzutów"
plt.title(hist_title)
plt.xlabel("Liczba oczek")
plt.ylabel("Liczba wyrzuceń")
plt.show()

y_sum = []

for i in range(len(y)):
    y_sum.append(licz_sum(y, i + 1))

plt.plot(list(range(len(y_sum))), y_sum)
plot_title = "Wykres funkcji S(n) dla " + str(randint_number) + " rzutów kostką"
plt.title(plot_title)
plt.xlabel("n")
plt.ylabel("S(n)")
plt.grid(axis="y", alpha=0.7)
plt.show()
######################################################

#histogramy
plt.hist(kostki_dane,6, edgecolor="black")
plt.title("Histogram rzutu kostką dla 466 rzutów")
plt.xlabel("Liczba oczek")
plt.ylabel("Liczba wyrzuceń")
plt.ylim(0,100)
plt.grid(axis="y", alpha=0.7)
plt.show()

plt.hist(wierzcholki, 40, edgecolor="black")
plt.grid(axis="y", alpha=0.7)
plt.ylim(0,30)
hist_title = "Histogram wektora stopni wierzchołków, zestaw nr." + wierzcholki_path
plt.title(hist_title)
plt.xlabel("Liczba sąsiadów")
plt.ylabel("Liczba powtórzeń")
plt.show()


#box plots
plt.boxplot(kostki_dane)
plt.title("Wykres skrzyniowy dla rzutu kostką")
plt.ylabel("Liczba oczek")
plt.grid(axis="y", alpha=0.7)
plt.show()

plt.boxplot(wierzcholki)
plot_title = "Wykres skrzyniowy dla wektora stopni wierzchołków, zestaw nr." + wierzcholki_path
plt.title(plot_title)
plt.ylabel("Liczba sąsiadów")
plt.grid(axis="y", alpha=0.7)
plt.show()


#S(n)
kostki_sum = []
wierzcholki_sum = []


for i in range(len(kostki_dane)):
    kostki_sum.append(licz_sum(kostki_dane, i + 1))
    #print(licz_sum(kostki_dane, i))
    #print(i)

for i in range(len(wierzcholki)):
    wierzcholki_sum.append(licz_sum(wierzcholki, i + 1))

plt.plot(list(range(466)), kostki_sum)
plt.title("Wykres funkcji S(n) dla rzutu kostką")
plt.xlabel("n")
plt.ylabel("S(n)")
plt.grid(axis="y", alpha=0.7)
plt.show()

plt.plot(list(range(len(wierzcholki_sum))), wierzcholki_sum)
plot_title = "Wykres funkcji S(n) dla zestawu nr." + wierzcholki_path
plt.title(plot_title)
plt.xlabel("n")
plt.ylabel("S(n)")
plt.grid(axis="y", alpha=0.7)
plt.show()